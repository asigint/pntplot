import json
from datetime import datetime
import numpy as np
import matplotlib.pyplot as plt
from os.path import split
import glob



in_dir = '/opt/asi/devel/PNTPlot/data/nav_raw/'
out_dir = '/opt/asi/devel/PNTPlot/plots/'

file_list = glob.glob(in_dir+'pnt_data*' )
file_list.sort()
print(file_list)




# Get data from file, convert to JSON
data = []
for file in file_list:
    with open(file, 'r') as fo:
        while True:
            try:
                line = fo.readline()
                try:
                    data.append(json.loads(line))
                except json.JSONDecodeError:
                    print(line)
                    line = fo.__next__()
                    continue
                line = fo.__next__()
            except StopIteration:
                break
for d in data:
    print(d)
# # Get yaw Data
# utc_times = []
# for d in data:
#     ts = d['PNT_DATA']['GPS']['utctime']
#     utc_ts = datetime.strptime(ts,time_format)
#     utc_times.append(utc_ts)
#
# diffs = []
# for x in np.arange(0,len(utc_times)-1,1):
#     try:
#         df = utc_times[x+1] - utc_times[x]
#         print(df)
#         diffs.append(df.total_seconds())
#     except IndexError as err:
#         print(err)
#         continue
# min_diff = min(diffs)
# max_diff = max(diffs)
# ave_diff = sum(diffs)/(len(diffs))
#
# t = np.arange(0,len(utc_times),1)
#
# t2 = np.arange(0, len(diffs),1)
#
# plt.subplot(211)
#
# plt.plot(t, utc_times, 'o', mfc='none')
# plt.xlabel('Samples, sample size : {}'.format(len(utc_times)))
# plt.ylabel('Timestamps - UTC')
# plt.suptitle('ASI PNT Application Data')
# plt.title(infile)
#
#
# plt.subplot(212)
# plt.plot(t2, diffs, 'ro', mfc='none')
# plt.xlabel('Samples, sample size : {}, min : {}, max : {}, ave: {}'.format(len(diffs), min_diff, max_diff, ave_diff))
# plt.ylabel('Timestamp difference, seconds')
#
#
#
#
# plt.savefig(out_dir+name+'_timestamps_UTC.png')
# plt.show()
