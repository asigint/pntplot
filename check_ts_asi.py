import json
from datetime import datetime
import numpy as np
import matplotlib.pyplot as plt
from os.path import split


#infile = '/opt/asi/devel/PNTPlot/data/pnt_data.2019-05-08_15-28'
#infile = '/opt/asi/devel/PNTPlot/data/PNT_test_2/pnt_data_client'
#infile = '/opt/asi/devel/PNTPlot/data/pnt_data_050919.log'
#infile = '/opt/asi/devel/PNTPlot/data/asism10/pnt_data.2019-05-10_12-40'
#infile = '/opt/asi/devel/PNTPlot/data/asism10/pnt_data.2019-05-10_12-35'
#infile = '/opt/asi/devel/PNTPlot/data/laptop_nostack/pnt_data.2019-05-13_18-51.log'
#infile = '/opt/asi/devel/PNTPlot/data/asism10/pnt_data_radio_2_19-05-15_16-20-08'
infile = '/opt/asi/devel/PNTPlot/data/asism10/pnt_data_radio_2_19-05-15_16-28-15'

location, name = split(infile)

out_dir = '/opt/asi/devel/PNTPlot/plots/'

time_format ='%Y-%m-%dT%H:%M:%S.%fZ'

# Get data from file, convert to JSON
data = []
with open(infile, 'r') as fo:
    while 1:
        try:
            line = fo.readline()
            try:
                data.append(json.loads(line))
            except json.JSONDecodeError:
                print(line)
                line = fo.__next__()
                continue
            line = fo.__next__()
        except StopIteration:
            break
print(len(data))
# Get yaw Data
utc_times = []
for d in data:
    ts = d['PNT_DATA']['GPS']['utctime']
    utc_ts = datetime.strptime(ts,time_format)
    utc_times.append(utc_ts)

diffs = []
for x in np.arange(0,len(utc_times)-1,1):
    try:
        df = utc_times[x+1] - utc_times[x]
        print(df)
        diffs.append(df.total_seconds())
    except IndexError as err:
        print(err)
        continue
min_diff = min(diffs)
max_diff = max(diffs)
ave_diff = sum(diffs)/(len(diffs))

t = np.arange(0,len(utc_times),1)

t2 = np.arange(0, len(diffs),1)

plt.subplot(211)

plt.plot(t, utc_times, 'o', mfc='none')
plt.xlabel('Samples, sample size : {}'.format(len(utc_times)))
plt.ylabel('Timestamps - UTC')
plt.suptitle('ASI PNT Application Data')
plt.title(infile)


plt.subplot(212)
plt.plot(t2, diffs, 'ro', mfc='none')
plt.xlabel('Samples, sample size : {}, min : {}, max : {}, ave: {}'.format(len(diffs), min_diff, max_diff, ave_diff))
plt.ylabel('Timestamp difference, seconds')




plt.savefig(out_dir+name+'_timestamps_UTC.png')
plt.show()
