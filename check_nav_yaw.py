import matplotlib.pyplot as plt
import numpy as np
from os.path import split
from datetime import datetime
import glob
from AsiNavigation.parse_vectornav_msg import parse_vectornav_msg


in_dir = '/opt/asi/devel/PNTPlot/data/nav_raw/'
out_dir = '/opt/asi/devel/PNTPlot/plots/'

file_list = glob.glob(in_dir+'*.log' )
file_list.sort()
print(file_list)

data = []
for file in file_list:

    with open(file, 'r') as fo:
        for line in fo:
            line = line.strip()
            line = line.split('\t')
            result = parse_vectornav_msg(line[1])
            data.append(result)


yaws = []

for d in data:
    yaw = d[0]['yaw_true']
    yaws.append(yaw)

t = np.arange(0, len(yaws), 1)
plt.plot(t, yaws, 'o', mfc='none')
plt.xlabel('Samples, sample size : {}'.format(len(yaws)))
plt.ylabel('Yaw - Degrees')
plt.suptitle('AsiNavigationServer Raw VINS Log')
plt.title('Raw Yaw')
plt.show()
