import matplotlib.pyplot as plt
import numpy as np
from os.path import split
from datetime import datetime
import glob

in_dir = '/opt/asi/devel/PNTPlot/data/nav_raw/'
out_dir = '/opt/asi/devel/PNTPlot/plots/'

file_list = glob.glob(in_dir+'*.log' )
file_list.sort()
print(file_list)

data = []
for file in file_list:

    with open(file, 'r') as fo:
        for line in fo:
            line = line.strip()
            line = line.split('\t')
            data.append(float(line[0]))

diffs = []
for x in np.arange(0, len(data) - 1, 1):
    try:
        df = data[x + 1] - data[x]
        diffs.append(df)
    except IndexError as err:
        print(err)
        continue

print(data[7055:7065])
min_diff = min(diffs)
max_diff = max(diffs)
ave_diff = sum(diffs)/(len(diffs))

t = np.arange(0, len(data), 1)

t2 = np.arange(0, len(diffs),1)
#
plt.subplot(211)

plt.plot(t, data, 'o', mfc='none')
plt.xlabel('Samples, sample size : {}'.format(len(data)))
plt.ylabel('Timestamps - Float')
plt.suptitle('AsiNavigationServer Raw VINS Log')
plt.title('System Timestamps ')


plt.subplot(212)
plt.plot(t2, diffs, 'ro', mfc='none')
plt.xlabel('Samples, sample size : {}, min : {}, max : {}, ave: {}'.format(len(diffs), min_diff, max_diff, ave_diff))
plt.ylabel('Timestamp difference, seconds')


plt.savefig(out_dir+'nav_logs_timestamps_float.png')
plt.show()