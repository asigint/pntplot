import json
import numpy as np
import matplotlib.pyplot as plt
#from scipy.stats import signaltonoise

#df = '/opt/asi/devel/PNTPlot/data/PNT_test_2/pnt_data_client'
#df = '/opt/asi/devel/PNTPlot/data/pnt_data_050919.log'
#df = '/opt/asi/devel/PNTPlot/data/pnt_data.2019-05-09_14-41.log'
df = '/opt/asi/devel/PNTPlot/data/pnt_data.2019-05-09_15-04.log'

out_dir = '/opt/asi/devel/PNTPlot/plots/'

# Get data from file, convert to JSON
data = []
with open(df, 'r') as fo:
    while 1:
        try:
            err_cnt = 0
            line = fo.readline()
            try:
                data.append(json.loads(line))
            except json.JSONDecodeError:
                err_cnt += 1
                pass
            line = fo.__next__()
        except StopIteration:
            break
print('Read {} with : {} errors.'.format(df,err_cnt))
# Get yaw Data
yaw = []
for d in data:
    yaw.append(d['PNT_DATA']['COMPASS']['yaw_true'])



#sn = signaltonoise(yaw)
#print(sn)

t = np.arange(0,len(yaw),1)

plt.plot(t,yaw)

plt.xlabel('Sample sample size : {}'.format(len(yaw)))
plt.ylabel('yaw_true'.format())
plt.title('ASI PNT Application Data')
plt.suptitle(df)
plt.savefig(out_dir+'pnt_data_client.png')
plt.show()

