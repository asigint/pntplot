import matplotlib.pyplot as plt
import numpy as np
from os.path import split
from datetime import datetime

df1 = '/opt/asi/devel/PNTPlot/data/PNT_test_2/pnt_cntl_toughbook'
out_dir = '/opt/asi/devel/PNTPlot/plots/'

time_format ='%Y-%m-%dT%H:%M:%S.%f'

location, input_file = split(df1)


with open(df1,'r' )as infile:
    header = infile.readline()

    data = []
    while True:
        line = infile.readline()
        if not line:
            break
        data.append(line)

header = header.split('\t')

nh = []
for x in header:
    nh.append(x.replace(' - Sensor A (VectorNav.Vn300ThermalRugged.0100033899)','',1))

gps_times = []

for y in data:
    y = y.split('\t')
    ts = utc_ts = datetime.strptime(y[0],time_format)
    gps_times.append(ts)

diffs = []
for x in np.arange(0, len(gps_times) - 1, 1):
    try:
        df = gps_times[x + 1] - gps_times[x]
        print(df)
        diffs.append(df.total_seconds())
    except IndexError as err:
        print(err)
        continue


min_diff = min(diffs)
max_diff = max(diffs)
ave_diff = sum(diffs)/(len(diffs))

t = np.arange(0, len(gps_times), 1)

t2 = np.arange(0, len(diffs),1)

plt.subplot(211)

plt.plot(t, gps_times, 'o', mfc='none')
plt.xlabel('Samples, sample size : {}'.format(len(gps_times)))
plt.ylabel('Timestamps - UTC')
plt.suptitle('Vector Nav Application Data')
plt.title(input_file)


plt.subplot(212)
plt.plot(t2, diffs, 'ro', mfc='none')
plt.xlabel('Samples, sample size : {}, min : {}, max : {}, ave: {}'.format(len(diffs), min_diff, max_diff, ave_diff))
plt.ylabel('Timestamp difference, seconds')


plt.savefig(out_dir+input_file+'_timestamps_UTC.png')
plt.show()