import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import signaltonoise
df1 = '/opt/asi/devel/PNTPlot/data/PNT_test_2/pnt_cntl_toughbook'
out_dir = '/opt/asi/devel/PNTPlot/plots/'

with open(df1,'r' )as infile:
    header = infile.readline()

    data = []
    while True:
        line = infile.readline()
        if not line:
            break
        data.append(line)

header = header.split('\t')

nh = []
for x in header:
    nh.append(x.replace(' - Sensor A (VectorNav.Vn300ThermalRugged.0100033899)','',1))

yaw = []

for y in data:
    y = y.split('\t')
    yaw.append(np.float64(y[1]))
sn = signaltonoise(yaw)
print(sn)
t = np.arange(0,len(yaw),1)

plt.plot(t,yaw)

plt.xlabel('Sample, sample size : {}'.format(len(yaw)))
plt.ylabel(nh[1]+' snr : {}'.format(sn))
plt.title('Vector NAV Application Data')
plt.suptitle(df1)
plt.savefig(out_dir+'pnt_cntl_toughbook.png')
plt.show()


